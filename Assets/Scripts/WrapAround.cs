﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WrapAround : MonoBehaviour
{
    private Camera cam;
    private Rigidbody2D rb;

    void Start()
    {
        cam = Camera.main;
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 viewportPosition = cam.WorldToViewportPoint(rb.position);
        Vector3 newPosition = rb.position;

        if (viewportPosition.x > 1)
        {
            newPosition.x = cam.ViewportToWorldPoint(new Vector3(0, viewportPosition.y, viewportPosition.z)).x;
        }
        else if (viewportPosition.x < 0)
        {
            newPosition.x = cam.ViewportToWorldPoint(new Vector3(1, viewportPosition.y, viewportPosition.z)).x;
        }

        if (viewportPosition.y > 1)
        {
            newPosition.y = cam.ViewportToWorldPoint(new Vector3(viewportPosition.x, 0, viewportPosition.z)).y;
        }
        else if (viewportPosition.y < 0)
        {
            newPosition.y = cam.ViewportToWorldPoint(new Vector3(viewportPosition.x, 1, viewportPosition.z)).y;
        }

        rb.position = newPosition;
    }
}
