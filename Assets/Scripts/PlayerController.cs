﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float time;
    public float fireWait;
    public float thrust;
    public float maxControlledVelocity;
    public float decelerationPerTick;
    public ObjectPool bulletPool;
    public Vector2[] barrels;
    private Rigidbody2D rb;
    private Vector2 aimDir;
	private Vector2 lastMousePos;
    private int barrelNum;
    private AudioSource gunSoundSource;
    private bool stickControlled;
    private Camera cam;
    private float camOffset;
    private EnergyManager energyManager;
    // input bools
    private bool fire1;
	private bool changeMenuState;
	private GameObject pauseMenu;

	private Dictionary<string, ControlBinding> keys;
	private enum KeyState { Down, Up, Held };

	void Awake()
	{
        time = Time.time;
        // After some resarch, joystick axes work differently. Needs more looking into if we want to bind those.
        // Probably needed for XBox360 Triggers, as they're analog.
        // This lets us programmatically have multiple key bindings per control.
        // And will let us change them at runtime in future.
        keys = new Dictionary<string, ControlBinding> ();
		keys.Add ("Fire1", new ControlBinding { KeyCode.LeftControl, KeyCode.Mouse0, KeyCode.Joystick1Button5 /* Right Bumper */, KeyCode.Joystick1Button0 /* A Button */ });
		keys.Add ("Menu", new ControlBinding { KeyCode.Escape, KeyCode.Joystick1Button7 /* Start */ });
		keys.Add ("Fire2", new ControlBinding { KeyCode.LeftAlt, KeyCode.Mouse1, KeyCode.Joystick1Button4 /* Left Bumper */, KeyCode.Joystick1Button1 /* B Button */ });
		//keys.Add ("Fire3", new ControlBinding { KeyCode.LeftShift, KeyCode.Mouse2, KeyCode.Joystick1Button2 /* X Button */ });
		//keys.Add ("Cancel", new ControlBinding { KeyCode.Escape, KeyCode.Joystick1Button1 /* B Button */ });
		//keys.Add ("Submit", new ControlBinding { KeyCode.Return, KeyCode.KeypadEnter, KeyCode.Space, KeyCode.Joystick1Button0 /* A Button */ });
	}

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        gunSoundSource = GetComponents<AudioSource>()[0];
        barrelNum = 0;
        fire1 = false;
		changeMenuState = false;
        aimDir = Vector2.up;
        lastMousePos = Input.mousePosition;
        stickControlled = false;
        cam = Camera.main;
        camOffset = -cam.transform.position.z;
		pauseMenu = GameObject.FindGameObjectWithTag ("Pause Menu");
		pauseMenu.SetActive(false); // Start off.
        energyManager = GetComponent<EnergyManager>();
    }

	public ControlBinding GetControlBinding(string bindingName)
	{
		return keys [bindingName];
	}

	public void ClearControlBinding(string bindingname)
	{
		keys [bindingname].Clear ();
	}

	public bool AddControlBinding(string bindingName, KeyCode key)
	{
		if (keys.ContainsKey (bindingName)) {
			if (!keys [bindingName].Contains (key)) {
				keys [bindingName].Add (key);
				return true;
			}
			// Do nothing if it's already bound.
		} else {
			// If there's no such control, make one, and add a binding for it.
			keys.Add (bindingName, new ControlBinding { key });
			return true;
		}
		return false;
	}

	bool CheckControlBinding(ControlBinding binding, KeyState keystate = KeyState.Down)
	{
		switch (keystate) {
		case KeyState.Down:
			foreach (KeyCode key in binding) {
				if (Input.GetKeyDown (key)) {
					return true;
				}
			}
			break;
		case KeyState.Up:
			foreach (KeyCode key in binding) {
				if (Input.GetKeyUp (key)) {
					return true;
				}
			}
			break;
		case KeyState.Held:
			foreach (KeyCode key in binding) {
				if (Input.GetKey (key)) {
					return true;
				}
			}
			break;
		}
			

		return false;
	}

    // Update is called once per frame
    void Update()
	{
        // grab both input types for aiming
		Vector2 stickDir = Vector2.ClampMagnitude (new Vector2 (
			                       Input.GetAxisRaw ("Joy1RX"), Input.GetAxisRaw ("Joy1RY")), 1);
        Vector2 mousePos = Input.mousePosition;

        if (stickDir.magnitude != 0) // if a joystick is making input, prioritize it over mouse
        {
            aimDir = stickDir;
            stickControlled = true;
        }
        else if (!stickControlled || mousePos != lastMousePos) // if no joystick input is made and a mouse input IS made, check its position
        {
            // convert screen point to world point
            Vector3 viewportPoint = cam.ScreenToViewportPoint(mousePos);
            viewportPoint.z = camOffset;
            Vector3 worldPoint = cam.ViewportToWorldPoint(viewportPoint);
#if UNITY_EDITOR
            // draw a line on the debug screen showing where the mouse is
            Debug.DrawLine(rb.position, worldPoint);
#endif
            aimDir = (Vector2)worldPoint - rb.position;

            stickControlled = false;
        }

        // rotate the ship to face the aim direction
        rb.rotation = -Mathf.Atan2(aimDir.x, aimDir.y) * Mathf.Rad2Deg;

        // set up historical mouse position for next frame
        lastMousePos = mousePos;

		// handle button inputs
		if (CheckControlBinding(keys["Fire1"])) {
			fire1 = true;
		}

		if(CheckControlBinding(keys["Menu"])) {
			changeMenuState = true;
		}

		if (GameManager.IsGamePaused &&
			CheckControlBinding (keys ["Fire1"], KeyState.Held) &&
			CheckControlBinding (keys ["Fire2"], KeyState.Held)) {
			Application.Quit ();
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#endif
		}

		if (changeMenuState) {
			changeMenuState = false;
			pauseMenu.SetActive (!pauseMenu.activeSelf);
			if(pauseMenu.activeSelf) {
				GameManager.Pause ();
			} else {
				GameManager.Continue ();
			}
		}
	}

    // FixedUpdate is called every physics tick
    void FixedUpdate()
    {
        // get direction of user's input
        // clamps to 1 in case of diagonal movement
        Vector2 moveDir = Vector2.ClampMagnitude(new Vector2(
            Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")), 1);

        float percent = moveDir.magnitude;

        if (moveDir.magnitude > 0)
        {
            percent = energyManager.SpendThrustEnergy(percent);
        }

        /* movement handling */
        if (moveDir == Vector2.zero)
        {
            // decelerate if no input
			rb.velocity *= 1 - decelerationPerTick;
        }
        else
        {
			rb.AddForce(moveDir * thrust * percent);

            // clamp velocity to max speed
            rb.velocity = Vector2.ClampMagnitude(rb.velocity, maxControlledVelocity);
        }

        /* weapon handling */
        if (fire1 && Time.time >= (time + fireWait))
        {
            if (energyManager.SpendBulletEnergy())
            {
                gunSoundSource.Play();
                Poolable obj = bulletPool.Pop();
                BulletBehaviour bullet = obj.GetComponent<BulletBehaviour>();

                // sin and cosine for current aim direction
                float sin = Mathf.Sin(rb.rotation * Mathf.Deg2Rad);
                float cos = Mathf.Cos(rb.rotation * Mathf.Deg2Rad);

                // find offset for gun barrel
                Vector2 b = barrels[barrelNum];
                Vector2 offset = new Vector2(b.x * cos - b.y * sin,
                        b.x * sin + b.y * cos);
                if (++barrelNum >= barrels.Length)
                {
                    barrelNum = 0;
                }

                // fire bullet from offset position
                bullet.transform.position = rb.position + offset;
                bullet.Fire(aimDir);
                time = Time.time;
            }

            // reset input bool
            fire1 = false;
        }
    }
}