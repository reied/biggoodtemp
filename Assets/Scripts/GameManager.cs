﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager {

	private static bool isGamePaused;
	public static bool IsGamePaused { get { return isGamePaused; } }

	public static void Pause () {
		isGamePaused = true;
		Time.timeScale = 0;
	}

	public static void Continue () {
		isGamePaused = false;
		Time.timeScale = 1;
	}
}