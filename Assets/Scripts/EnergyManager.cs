﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyManager : MonoBehaviour
{
    /// <summary>
    /// Amount of energy at start.
    /// </summary>
    public float maxEnergy;
    /// <summary>
    /// Energy regenerated per second.
    /// </summary>
    public float regenPerSecond;
    /// <summary>
    /// Delay in seconds before energy starts regenerating after last use.
    /// </summary>
    public float delaySeconds;
    /// <summary>
    /// Amount of energy spent per bullet.
    /// </summary>
    public float bulletCost;
    /// <summary>
    /// Initially represents amount of energy spent per second of thrust.
    /// Value is changed during runtime to represent amount of energy spent per tick of thrust.
    /// </summary>
    public float thrustCost;
    /// <summary>
    /// Amount of energy spent on collision with an enemy.
    /// </summary>
    public float collisionCost;

    private float energy;
    private bool regen;
    private Coroutine regenCoroutine;
    private AudioSource explosionSource;
    private AudioSource collisionSource;
    private AudioSource powerUpPickup;

    // Use this for initialization
    private void Start()
    {
        energy = maxEnergy;
        regen = true;
        collisionSource = GetComponents<AudioSource>()[1];
        powerUpPickup = GetComponents<AudioSource>()[2];
        explosionSource = GameObject.Find("ExplosionManager").GetComponents<AudioSource>()[0];
        thrustCost *= Time.fixedDeltaTime;
    }

    private void Update()
    {
        if (regen)
        {
            energy += regenPerSecond * Time.deltaTime;
            if (energy > maxEnergy)
            {
                energy = maxEnergy;
            }
        }

        Debug.Log(energy); // TODO: remove this when energy UI is available.
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag.Equals("Asteroid"))
        {
            collisionSource.Play();
            energy -= collisionCost;
        }

        if (energy <= 0)
        {
            explosionSource.Play();
            gameObject.SetActive(false); // TEMP BEHAVIOUR
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        powerUpPickup.Play();
        if (col.CompareTag("EnergyPowerUp"))
        {
            EnergyPowerUpBehavior powerUpBehaviour = col.GetComponent<EnergyPowerUpBehavior>();
            energy += powerUpBehaviour.healAmt; 
            if (energy > maxEnergy)
            {
                energy = maxEnergy;
            }
            powerUpBehaviour.GetPickedUp();
        }
        else if (col.CompareTag("SpeedPowerUp"))
        {
            SpeedPowerUpBehavior powerUpBehaviour = col.GetComponent<SpeedPowerUpBehavior>();
            StartCoroutine(powerUpBehaviour.Pickup(gameObject.GetComponent<Collider2D>()));
            powerUpBehaviour.GetPickedUp();
        }
        else if (col.CompareTag("ROFPowerUp"))
        {
            ROFPowerUpBehavior powerUpBehaviour = col.GetComponent<ROFPowerUpBehavior>();
            powerUpBehaviour.Effect(gameObject);
            powerUpBehaviour.GetPickedUp();
        }
    }

    /// <summary>
    /// Checks if a bullet can be fired, then spends the energy amount.
    /// </summary>
    /// <returns>Whether the energy cost of a bullet was available.</returns>
    public bool SpendBulletEnergy()
    {
        // guard clause: return false if not enough energy available, prevents regen delay from restarting
        if (energy < bulletCost)
        {
            return false;
        }

        StartRegenDelay();

        energy -= bulletCost;
        if (energy < 0)
        {
            energy = 0;
        }

        return true;
    }

    /// <summary>
    /// Uses up an amount of thrust energy, based off of the percentage of thrust
    /// requested and amount of fuel available.
    /// </summary>
    /// <param name="percent">Percentage of maximum thrust requested.</param>
    /// <returns>Percentage of maximum thrust possible.</returns>
    public float SpendThrustEnergy(float percent)
    {
        // guard clause: return 0 if no energy available, prevents regen delay from restarting
        if (energy == 0)
        {
            return 0;
        }

        StartRegenDelay();

        if (energy < thrustCost)
        {
            energy = 0;
            return energy / thrustCost * percent;
        }
        else
        {
            energy -= thrustCost * percent;
            return 1;
        }
    }

    /// <summary>
    /// Starts the regen delay coroutine, which temporarily stops energy from regenerating.
    /// </summary>
    private void StartRegenDelay()
    {
        if (regenCoroutine != null)
        {
            StopCoroutine(regenCoroutine);
        }
        regenCoroutine = StartCoroutine(RegenDelay());
    }

    /// <summary>
    /// Disables energy regeneration for the number of seconds in delaySeconds.
    /// </summary>
    /// <returns>IEnumerator used by coroutine.</returns>
    private IEnumerator RegenDelay ()
    {
        regen = false;
        yield return new WaitForSeconds(delaySeconds);
        regen = true;
    }
}
